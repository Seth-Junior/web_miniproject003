import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import {
  Table,
  Container,
  Row,
  Col,
  Form,
  Button,
  ButtonGroup,
} from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchAllAuthors,
  postAuthor,
  deleteAuthor,
} from "../redux/actions/authorAction";

const placeholderImage = "../images/placeholder.png";

export default function Authors() {
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [imageUrl, setImageUrl] = useState(placeholderImage);
  const [imageFile, setImageFile] = useState(null);
  const [reload, setReload] = useState(false);

  const [isUpdating, setIsUpdating] = useState(false);

  const dispatch = useDispatch();
  const { authors } = useSelector((state) => state.authorReducer);

  useEffect(() => {
    dispatch(fetchAllAuthors());
    if (reload) setReload(false);
  }, [reload]);

  const onAddAuthor = async (e) => {
    e.preventDefault();
    const author = {
      name,
      email,
      image: imageUrl,
    };
    await dispatch(postAuthor(author, imageFile));
    setReload(true);
  };

  const onEdit = (e, author) => {
    e.preventDefault();
    setId(author._id);
    setName(author.name);
    setEmail(author.email);
    setImageUrl(author.image);
  };

  const onDelete = (e, id) => {
    e.preventDefault();
    dispatch(deleteAuthor(id));
    setReload(true);
  };

  const onSelectImage = (e) => {
    const url = URL.createObjectURL(e.target.files[0]);
    setImageUrl(url);
    setImageFile(e.target.files[0]);
  };

  return (
    <>
      <Container>
        <h1 className={"mt-5"}>Authors</h1>
        <Row>
          <Col xs={8}>
            <Form>
              <Form.Group controlId='formBasicEmail' className={"mt-4"}>
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type='text'
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Group>
              <Form.Group controlId='formBasicPassword' className={"mt-4"}>
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type='email'
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </Form.Group>
              <Button
                variant='primary'
                type='submit'
                className={"mt-5"}
                onClick={(e) => {
                  setIsUpdating(false);
                  onAddAuthor(e);
                }}>
                {isUpdating ? "Save" : "Submit"}
              </Button>
            </Form>
          </Col>
          <Col xs={4}>
            <img
              src={imageUrl}
              alt='no placeholder image'
              width='300px'
              className={"mx-auto mt-5"}
            />
            <Form.Group controlId='formFile' className='mt-3'>
              <Form.Control type='file' onChange={(e) => onSelectImage(e)} />
            </Form.Group>
          </Col>
        </Row>{" "}
          <Table bordered hover className={"mt-5"}>
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Image</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {authors.map((author, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{author.name}</td>
                  <td>{author.email}</td>
                  <th>
                    <img src={author.image} width='200px' />
                  </th>
                  <td>
                    <ButtonGroup aria-label='Basic example'>
                      <Button
                        variant='warning'
                        onClick={(e) => {
                          if (isUpdating) {
                            setIsUpdating(false);
                          } else {
                            setIsUpdating(true);
                            onEdit(e, author);
                          }
                        }}>
                        Edit
                      </Button>
                      <Button
                        variant='danger'
                        disabled={isUpdating}
                        onClick={(e) => onDelete(e, author._id)}>
                        Delete
                      </Button>
                    </ButtonGroup>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
      </Container>
    </>
  );
}
