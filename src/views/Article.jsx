import React, { useState, useEffect } from "react";
import { Container, Form, Button, Row, Col, Dropdown } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { postArticle } from "../redux/actions/articleAction";
import { Redirect } from "react-router";
import { fetchAllAuthors } from "../redux/actions/authorAction";
import { fetchAllCategories } from "../redux/actions/categoryAction";

const placeholderImage = "../images/placeholder.png";

export default function NewArticle() {
  const [authorId, setAuthorId] = useState("");
  const [authorName, setAuthorName] = useState("");

  const [categoryId, setCategoryId] = useState("");

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [imageUrl, setImageUrl] = useState(placeholderImage);
  const [imageFile, setImageFile] = useState(null);
  const [uploadedArticle, setUploadedArticle] = useState(false);

  const dispatch = useDispatch();
  const uploaded = useSelector((state) => state.uploaded);
  const { authors } = useSelector((state) => state.authorReducer);

  useEffect(() => {
    setUploadedArticle(uploaded);
    dispatch(fetchAllAuthors());
    dispatch(fetchAllCategories());
  }, [uploadedArticle]);

  const onSelectImage = (e) => {
    const url = URL.createObjectURL(e.target.files[0]);
    setImageUrl(url);
    setImageFile(e.target.files[0]);
  };

  const onAddArticle = async (e) => {
    e.preventDefault();
    const article = {
      title,
      description,
      published: true,
      category: categoryId,
      author: authorId,
    };
    dispatch(postArticle(article, imageFile));
    setUploadedArticle(true);
  };

  return (
    <div className={"d-flex flex-column"}>
      {uploadedArticle ? (
        <Redirect to={"/"} />
      ) : (
        <Container>
          <h1 className={"mt-5"}>Add Article</h1>
          <Row>
            <Col xs={8}>
              <Form>
                <Form.Group controlId='formBasicEmail' className={"mt-4"}>
                  <Form.Label>Title</Form.Label>
                  <Form.Control
                    type='text'
                    onChange={(e) => setTitle(e.target.value)}
                  />
                </Form.Group>
                <Form.Group controlId='formBasicEmail' className={"mt-4"}>
                  <Form.Label>Author</Form.Label>
                  <Dropdown className={"w-100"}>
                    <Dropdown.Toggle variant='light' id='dropdown-basic'>
                      {authorName ? authorName : "Select Author Name"}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {authors.map((author, index) => (
                        <Dropdown.Item
                          key={index}
                          onClick={() => {
                            setAuthorId(author._id);
                            setAuthorName(author.name);
                          }}>
                          {author.name}
                        </Dropdown.Item>
                      ))}
                    </Dropdown.Menu>
                  </Dropdown>
                </Form.Group>{" "}
                <Form.Group
                  className='mt-4'
                  controlId='exampleForm.ControlTextarea1'>
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    as='textarea'
                    rows={3}
                    onChange={(e) => setDescription(e.target.value)}
                  />
                </Form.Group>
                <Button
                  variant='primary'
                  type='submit'
                  className={"mt-5"}
                  onClick={(e) => onAddArticle(e)}>
                  Submit
                </Button>
              </Form>
            </Col>
            <Col xs={4}>
              <img
                src={imageUrl}
                alt='no placeholder image'
                width='300px'
                className={"mx-auto mt-5"}
              />
              <Form.Group controlId='formFile' className='mt-3'>
                <Form.Control type='file' onChange={(e) => onSelectImage(e)} />
              </Form.Group>
            </Col>
          </Row>
        </Container>
      )}
    </div>
  );
}
