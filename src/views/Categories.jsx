import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import {
  Table,
  Container,
  Row,
  Col,
  Form,
  Button,
  ButtonGroup,
} from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchAllCategories,
  postCategory,
  deleteCategory,
  updateCategory,
} from "../redux/actions/categoryAction";

export default function Categories() {
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [reload, setReload] = useState(false);

  const [isUpdating, setIsUpdating] = useState(false);

  const dispatch = useDispatch();
  const { categories } = useSelector((state) => state.categoryReducer);

  useEffect(() => {
    dispatch(fetchAllCategories());
    if (reload) setReload(false);
  }, [reload]);

  const onAddCategory = (e) => {
    e.preventDefault();
    const category = {
      name,
    };
    if (isUpdating) {
      dispatch(updateCategory(id, category));
    } else {
      dispatch(postCategory(category));
    }
    setReload(true);
  };

  const onEdit = (e, category) => {
    e.preventDefault();
    setId(category._id);
    setName(category.name);
  };

  const onDelete = (e, id) => {
    e.preventDefault();
    dispatch(deleteCategory(id));
    setReload(true);
  };

  return (
    <>
      <Container className={"w-50"}>
        <h1 className={"mt-5"}>Categories</h1>
        <Row>
          <Col xs={12}>
            <Form>
              <Form.Group controlId='formBasicEmail' className={"mt-4"}>
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type='text'
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Group>
              <Button
                variant='primary'
                type='submit'
                className={"mt-5"}
                onClick={(e) => {
                  setIsUpdating(false);
                  onAddCategory(e);
                }}>
                {isUpdating ? "Save" : "Submit"}
              </Button>
            </Form>
          </Col>
        </Row>{" "}
          <Table bordered hover className={"mt-5"}>
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {categories.map((category, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td width='80%'>{category.name}</td>
                  <td>
                    <ButtonGroup aria-label='Basic example'>
                      <Button
                        variant='warning'
                        onClick={(e) => {
                          if (isUpdating) {
                            setIsUpdating(false);
                          } else {
                            setIsUpdating(true);
                            onEdit(e, category);
                          }
                        }}>
                        Edit
                      </Button>
                      <Button
                        variant='danger'
                        disabled={isUpdating}
                        onClick={(e) => onDelete(e, category._id)}>
                        Delete
                      </Button>
                    </ButtonGroup>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
      </Container>
    </>
  );
}
