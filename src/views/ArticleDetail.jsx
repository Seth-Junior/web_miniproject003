import React, { useState, useEffect } from "react";
import { Container, Row, Col} from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
fetchArticleById,
} from "../redux/actions/articleAction";
import { fetchAllAuthors } from "../redux/actions/authorAction";
import { fetchAllCategories } from "../redux/actions/categoryAction";
import { useParams } from "react-router";
import "react-toastify/dist/ReactToastify.css";

const placeholderImage = "../images/placeholder.png";

export default function ArticleDetail() {

const { id } = useParams();
const dispatch = useDispatch();
const { article } = useSelector((state) => state.articleReducer);

const [articleTitle, setArticleTitle] = useState("");
const [articleDescription, setArticleDescription] = useState("");
const [imageUrl, setImageUrl] = useState(placeholderImage);

let {description, image, title,} = {
    ...article,
};
useEffect(() => {
    dispatch(fetchArticleById(id));
    dispatch(fetchAllAuthors());
    dispatch(fetchAllCategories());
}, []);

useEffect(() => {
    setArticleTitle(title);
    setArticleDescription(description);
    setImageUrl(image);
}, [dispatch]);


return (
    <div className={"mx-auto mt-5"}>
    <Container>
        <Row>
            <Col xs={4}>
                <img
                src={imageUrl}
                alt='no placeholder image'
                width='100%'
                
                />
            </Col>
            <Col xs={8} >
                <h3>{articleTitle}</h3><br/>
                <h6>Description</h6><br/>
                {articleDescription}
            </Col>
        </Row>
        <a href="/" >Back Home</a>
    </Container>
    </div>
);
}
