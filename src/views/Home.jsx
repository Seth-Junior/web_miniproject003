import React, { useEffect, useState } from "react";
import { Button, Container, Card, ButtonGroup } from "react-bootstrap";
import {
  fetchArticlesByPage,
  deleteArticle,
} from "../redux/actions/articleAction";
import { useDispatch, useSelector } from "react-redux";
import { Redirect , useHistory} from "react-router";
import ReactPaginate from "react-paginate";


export const Home = () => {
  const [isUpdating, setIsUpdating] = useState(false);
  const [willReload, setWillReload] = useState(false);
  const [id, setId] = useState("");

  const history = useHistory();
  const [page, setPage] = useState(1);
  const [size] = useState(7);

  const dispatch = useDispatch();
  const { articles, count } = useSelector((state) => state.articleReducer);

  useEffect(async () => {
    dispatch(fetchArticlesByPage(page, size));
    setWillReload(false);
  }, [dispatch, willReload, page]);

  const onDelete = (e, id) => {
    e.preventDefault();
    dispatch(deleteArticle(id));
    setWillReload(true);
  };

  const onUpdate = (e, id) => {
    e.preventDefault();
    setIsUpdating(true);
    setId(id);
  };

  const onPageChange = ({ selected: selectedPage }) => {
    setPage(selectedPage + 1);
  };
  return (
    <div>
      {isUpdating ? (
        <Redirect to={`/update/article/${id}`} />
      ) : (
        <Container>
            <div className='d-flex flex-row flex-wrap'>
              {articles ? (
                articles.map((article, index) => (
                  <Card style={{ width: "16rem" }} key={index}>
                    <Card.Img variant='top' src={article.image} />
                    <Card.Body>
                      <Card.Title>{article.title}</Card.Title>
                      <Card.Text>{article.author ? "jes" : "joe"}</Card.Text>
                      <ButtonGroup aria-label='Basic example'>
                        <Button variant='primary' onClick={()=>history.push('/view_atricle/'+article._id)}>View</Button>
                        <Button
                          variant='warning'
                          onClick={(e) => onUpdate(e, article._id)}>Edit</Button>
                        <Button
                          variant='danger'
                          onClick={(e) => onDelete(e, article._id)}>Delete</Button>
                      </ButtonGroup>
                    </Card.Body>
                  </Card>
                ))
                ) : (
                  <img
                  src='../images/nodata.png'
                  width='100%'
                  alt='no data from api'
                  />
                  )}
            </div>
            <br/>
            <ReactPaginate
              pageCount={Math.ceil(count / size)}
              onPageChange={onPageChange}
              containerClassName={"pagination"}
              pageLinkClassName='page-link'
              nextLinkClassName='page-link'
              previousClassName='page-link'
              activeClassName="active"
            />
        </Container>
      )}
    </div>
  );
};

export default Home;
