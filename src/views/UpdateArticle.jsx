import React, { useState, useEffect } from "react";
import { Container, Form, Button, Row, Col, Dropdown } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  updateArticle,
  fetchArticleById,
} from "../redux/actions/articleAction";
import { fetchAllAuthors } from "../redux/actions/authorAction";
import { fetchAllCategories } from "../redux/actions/categoryAction";
import { Redirect, useParams } from "react-router";
import "react-toastify/dist/ReactToastify.css";

const placeholderImage = "../images/placeholder.png";

export default function UpdateArticle() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const { article } = useSelector((state) => state.articleReducer);
  const { authors } = useSelector((state) => state.authorReducer);
  const { categories } = useSelector((state) => state.categoryReducer);
  const [authorId, setAuthorId] = useState("");
  const [authorName, setAuthorName] = useState("");

  const [categoryId, setCategoryId] = useState("");
  const [categoryName, setCategoryName] = useState("");

  const [articleTitle, setArticleTitle] = useState("");
  const [articleDescription, setArticleDescription] = useState("");
  const [imageUrl, setImageUrl] = useState(placeholderImage);
  const [imageFile, setImageFile] = useState(null);
  const [redirectHome, setRedirectHome] = useState(false);

  let { author, category, description, image, published, title, _id } = {
    ...article,
  };
  useEffect(() => {
    dispatch(fetchArticleById(id));
    dispatch(fetchAllAuthors());
    dispatch(fetchAllCategories());
  }, []);

  useEffect(() => {
    setArticleTitle(title);
    setArticleDescription(description);
    setImageUrl(image);
    setCategoryId(category ? category._id : "");
    setCategoryName(category ? category.name : "");
    setAuthorId(author ? author._id : "");
    setAuthorName(author ? author.name : "");
  }, [dispatch]);

  const onUpdateArticle = (e) => {
    e.preventDefault();

    const item = {
      title: articleTitle,
      description: articleDescription,
      published: published,
      category: categoryId,
      author: authorId,
      image: imageUrl,
    };
    dispatch(updateArticle(_id, item, imageFile));
  };

  const onSelectImage = (e) => {
    const url = URL.createObjectURL(e.target.files[0]);
    setImageUrl(url);
    setImageFile(e.target.files[0]);
  };
  return (
    <div>
      <Container>
        <h1 className={"mt-5"}>Update Article</h1>
        <Row>
          <Col xs={8}>
            <Form>
              <Form.Group controlId='formBasicEmail' className={"mt-4"}>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type='text'
                  value={articleTitle}
                  onChange={(e) => setArticleTitle(e.target.value)}
                />
              </Form.Group>
              <Form.Group controlId='formBasicEmail' className={"mt-4"}>
                <Form.Label>Author</Form.Label>
                <Dropdown className={"w-100"}>
                  <Dropdown.Toggle variant='light' id='dropdown-basic'>
                    {authorName ? authorName : "Select Author Name"}
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    {authors.map((author, index) => (
                      <Dropdown.Item
                        key={index}
                        onClick={() => {
                          setAuthorId(author._id);
                          setAuthorName(author.name);
                        }}>
                        {author.name}
                      </Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
              </Form.Group>
              <Form.Group
                className='mt-4'
                controlId='exampleForm.ControlTextarea1'>
                <Form.Label>Description</Form.Label>
                <Form.Control
                  as='textarea'
                  value={articleDescription}
                  rows={3}
                  onChange={(e) => setArticleDescription(e.target.value)}
                />
              </Form.Group>
              <Button
                variant='primary'
                type='submit'
                className={"mt-5"}
                onClick={(e) => onUpdateArticle(e)}>
                Save
              </Button>
            </Form>
          </Col>
          <Col xs={4}>
            <img
              src={imageUrl}
              alt='no placeholder image'
              width='300px'
              className={"mx-auto mt-5"}
            />
            <Form.Group controlId='formFile' className='mt-3'>
              <Form.Control type='file' onChange={(e) => onSelectImage(e)} />
            </Form.Group>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
