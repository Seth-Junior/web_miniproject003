import { api } from "../util/api";

export const fetch_all_articles = async () => {
  try {
    const result = await api.get("/articles");
    console.log("fetch all", result);
    return result.data.data;
  } catch (err) {
    console.log(err);
  }
};

export const fetch_articles_by_page = async (page, size) => {
  if (!page) page = 1;
  if (!size) size = 5;

  try {
    const result = await api.get(`/articles?page=${page}&size=${size}`);
    console.log("fetch page", result);
    return result.data.data;
  } catch (err) {
    console.log(err);
  }
};

export const fetch_article_by_id = async (id) => {
  try {
    const result = await api.get(`/articles/${id}`);
    console.log("fetch article", result);
    return result.data.data;
  } catch (err) {
    console.log(err);
  }
};

export const get_page_count = async () => {
  try {
    const result = await api.get(`/articles`);
    console.log("count", result);
    return result.data.data.length;
  } catch (err) {
    console.log(err);
  }
};

export const post_article = async (article) => {
  try {
    const result = await api.post("/articles", article);
    console.log("post", result);
    return result.data.message;
  } catch (error) {
    console.log(error);
  }
};

export const delete_article = async (id) => {
  try {
    const result = await api.delete(`/articles/${id}`);
    console.log("delete", result);
    return result.data.message;
  } catch (error) {
    console.log(error);
  }
};

export const update_article = async (id, article) => {
  try {
    const result = await api.patch(`/articles/${id}`, article);
    console.log("update", result);
    return result.data.message;
  } catch (error) {
    console.log(error);
  }
};
