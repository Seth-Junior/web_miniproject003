import { api } from "../util/api";

export const upload_image = async (file) => {
  try {
    let formData = new FormData();
    formData.append("image", file);

    const result = await api.post("/images", formData);

    return result.data.url;
  } catch (error) {
    console.log(error);
  }
};
