import { api } from "../util/api";

export const fetch_all_authors = async () => {
  try {
    const results = await api.get("/author");
    return results.data.data;
  } catch (error) {
    console.log(error);
  }
};

export const fetch_author_by_id = async (id) => {
  try {
    const result = await api.get(`/author/${id}`);
    return result.data.data;
  } catch (err) {
    console.log(err);
  }
};

export const post_author = async (author) => {
  try {
    const result = await api.post("/author", author);
    return result.data.message;
  } catch (error) {
    console.log(error);
  }
};

export const delete_author = async (id) => {
  try {
    const result = await api.delete(`/author/${id}`);
    return result.data.message;
  } catch (error) {
    console.log(error);
  }
};

export const update_author = async (id, author) => {
  try {
    const result = await api.patch(`/author/${id}`, author);
    return result.data.message;
  } catch (error) {
    console.log(error);
  }
};
