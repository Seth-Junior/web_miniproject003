import { api } from "../util/api";

export const fetch_all_categories = async () => {
  try {
    const results = await api.get("/category");
    return results.data.data;
  } catch (error) {
    console.log(error);
  }
};

export const fetch_category_by_id = async (id) => {
  try {
    const result = await api.get(`/category/${id}`);
    return result.data.data;
  } catch (err) {
    console.log(err);
  }
};

export const post_category = async (category) => {
  try {
    const result = await api.post("/category", category);
    return result.data.message;
  } catch (error) {
    console.log(error);
  }
};

export const delete_category = async (id) => {
  try {
    const result = await api.delete(`/category/${id}`);
    return result.data.message;
  } catch (error) {
    console.log(error);
  }
};

export const update_category = async (id, category) => {
  try {
    const result = await api.put(`/category/${id}`, category);
    return result.data.message;
  } catch (error) {
    console.log(error);
  }
};
