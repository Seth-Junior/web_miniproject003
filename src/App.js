import Home from "./views/Home";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Navigation from "./components/Navigation";
import NewArticle from "./views/Article";
import UpdateArticle from "./views/UpdateArticle";
import Authors from "./views/Authors";
import Categories from "./views/Categories";
import ArticleDetail from "./views/ArticleDetail";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Navigation />
        <Switch>
          <Route exact path='/'>
            <Home />
          </Route>
          <Route path='/new/article'>
            <NewArticle />
          </Route>
          <Route path='/update/article/:id'>
            <UpdateArticle />
          </Route>
          <Route path='/authors'>
            <Authors />
          </Route>
          <Route path='/categories'>
            <Categories />
          </Route>
          <Route path='/view_atricle/:id'>
            <ArticleDetail />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
