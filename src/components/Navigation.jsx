import React from "react";
import {
  Navbar,
  Container,
  Nav,
  Form,
  FormControl,
  Button,
  NavDropdown,
} from "react-bootstrap";
import { NavLink } from "react-router-dom";
import './Comp.css'

export default function Navigation() {
  return (
    <div className="nav-compo">
      <Navbar>
        <Container>
          <Navbar.Brand href='#home'>AMS Redux</Navbar.Brand>
          <Nav className='mr-auto'>
            <Nav.Link as={NavLink} to='/'>
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to='/new/article'>
              Article
            </Nav.Link>
            <Nav.Link as={NavLink} to='/authors'>
              Author
            </Nav.Link>
            <Nav.Link as={NavLink} to='/categories'>
              Category
            </Nav.Link>
            <NavDropdown title='Language' id='basic-nav-dropdown'>
              <NavDropdown.Item href='#action/3.1'>Khmer</NavDropdown.Item>
              <NavDropdown.Item href='#action/3.2'>
                English
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Form inline>
            <div className='d-flex flex-row'>
              <FormControl
                type='text'
                placeholder='Search'
                className='mr-sm-2'
              />
              <Button variant='outline-primary'>Search</Button>
            </div>
          </Form>
        </Container>
      </Navbar>
    </div>
  );
}
