import { applyMiddleware, combineReducers, createStore } from "redux";
import { articleReducer } from "../reducers/articleReducer";
import { authorReducer } from "../reducers/authorReducer";
import { categoryReducer } from "../reducers/categoryReducer";
import thunk from "redux-thunk";

const rootReducer = combineReducers({
  articleReducer,
  authorReducer,
  categoryReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
