import * as TYPE from "../actions/authorAction";

const initialState = {
  authors: [],
};

export const authorReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case TYPE.FETCH_ALL_AUTHORS:
      console.log("reducer: ", payload);
      return {
        ...state,
        authors: payload,
      };

    case TYPE.FETCH_AUTHOR_BY_ID:
      return {
        authors: payload,
      };

    case TYPE.POST_AUTHOR:
      return {
        message: payload,
      };

    case TYPE.DELETE_AUTHOR:
      return {
        message: payload,
      };

    case TYPE.UPDATE_AUTHOR:
      return {
        message: payload,
      };

    default:
      return state;
  }
};
