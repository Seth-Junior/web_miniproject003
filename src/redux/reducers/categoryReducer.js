import {
  FETCH_ALL_CATEGORIES,
  FETCH_CATEGORY_BY_ID,
  DELETE_CATEGORY,
  POST_CATEGORY,
  UPDATE_CATEGORY,
} from "../actions/categoryAction";

const initialState = {
  categories: [],
};

export const categoryReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_ALL_CATEGORIES:
      return {
        ...state,
        categories: payload,
      };
    case FETCH_CATEGORY_BY_ID:
      return { ...state, cateogry: payload };

    case POST_CATEGORY:
      return { message: payload };

    case DELETE_CATEGORY:
      return { message: payload };

    case UPDATE_CATEGORY:
      return { message: payload };

    default:
      return state;
  }
};
