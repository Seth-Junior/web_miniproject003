import {
  FETCH_ALL_ARTICLES,
  FETCH_ARTICLE_BY_ID,
  FETCH_ARTICLES_BY_PAGE,
  DELETE_ARTICLE,
  POST_ARTICLE,
  UPDATE_ARTICLE,
} from "../actions/articleAction";

const initialState = {
  articles: [],
  count: 0,
};

export const articleReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_ALL_ARTICLES:
      return {
        ...state,
        articles: payload,
      };

    case FETCH_ARTICLES_BY_PAGE:
      return {
        ...state,
        articles: payload.articles,
        count: payload.count,
      };

    case FETCH_ARTICLE_BY_ID:
      return {
        article: payload,
      };

    case POST_ARTICLE:
      return {
        message: payload,
      };

    case DELETE_ARTICLE:
      return {
        message: payload,
      };

    case UPDATE_ARTICLE:
      return {
        message: payload,
      };

    default:
      return state;
  }
};
