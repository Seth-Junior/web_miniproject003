import {
  fetch_all_authors,
  post_author,
  update_author,
  delete_author,
  fetch_author_by_id,
} from "../../services/author_service";
import { upload_image } from "../../services/image_service";

export const FETCH_ALL_AUTHORS = "FETCH_ALL_AUTHORS";
export const FETCH_AUTHOR_BY_ID = "FETCH_AUTHOR_BY_ID";
export const POST_AUTHOR = "POST_AUTHOR";
export const DELETE_AUTHOR = "DELETE_AUTHOR";
export const UPDATE_AUTHOR = "UPDATE_AUTHOR";
export const LOAD_UPDATE_FORM = "LOAD_UPDATE_FORM";

export const fetchAllAuthors = () => {
  return async (dispatch) => {
    const authors = await fetch_all_authors();
    console.log("action: ", authors);
    dispatch({
      type: FETCH_ALL_AUTHORS,
      payload: authors,
    });
  };
};

export const fetchAuthor = (id) => {
  return async (dispatch) => {
    const author = await fetch_author_by_id(id);
    dispatch({
      type: FETCH_AUTHOR_BY_ID,
      payload: author,
    });
  };
};

export const postAuthor = (author, image) => {
  return async (dispatch) => {
    const url = await upload_image(image);

    author = {
      ...author,
      image: url,
    };

    const message = await post_author(author);
    alert(message);
    dispatch({
      type: POST_AUTHOR,
      payload: message,
    });
    return message;
  };
};

export const deleteAuthor = (id) => {
  return async (dispatch) => {
    const message = await delete_author(id);

    dispatch({
      type: DELETE_AUTHOR,
      payload: message,
    });
    return message;
  };
};

export const updateAuthor = (id, author, image) => {
  return async (dispatch) => {
    let url;
    if (author.image === undefined) {
      url = await upload_image(image);
    }
    author = {
      ...author,
      image: url,
    };
    const message = await update_author(id, author);
    dispatch({
      type: UPDATE_AUTHOR,
      payload: message,
    });
    return message;
  };
};
