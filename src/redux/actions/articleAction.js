import {
  delete_article,
  fetch_all_articles,
  fetch_articles_by_page,
  fetch_article_by_id,
  post_article,
  update_article,
} from "../../services/article_service";
import { upload_image } from "../../services/image_service";

export const FETCH_ALL_ARTICLES = "FETCH_ALL_ARTICLES";
export const FETCH_ARTICLE_BY_ID = "FETCH_ARTICLE_BY_ID";
export const POST_ARTICLE = "POST_ARTICLE";
export const DELETE_ARTICLE = "DELETE_ARTICLE";
export const UPDATE_ARTICLE = "UPDATE_ARTICLE";
export const FETCH_ARTICLES_BY_PAGE = "FETCH_ARTICLES_BY_PAGE";

export const fetchAllArticles = () => {
  return async (dispatch) => {
    const articles = await fetch_all_articles();
    console.log(articles);
    dispatch({
      type: FETCH_ALL_ARTICLES,
      payload: articles,
    });
  };
};

export const fetchArticlesByPage = (page, size) => {
  return async (dispatch) => {
    const articles = await fetch_articles_by_page(page, size);
    const allArticles = await fetch_all_articles();
    console.log(articles);
    dispatch({
      type: FETCH_ARTICLES_BY_PAGE,
      payload: { articles: articles, count: allArticles.length },
    });
  };
};

export const fetchArticleById = (id) => {
  return async (dispatch) => {
    const article = await fetch_article_by_id(id);
    dispatch({
      type: FETCH_ARTICLE_BY_ID,
      payload: article,
    });
  };
};

export const postArticle = (article, image) => {
  return async (dispatch) => {
    const url = await upload_image(image);
    article = {
      ...article,
      image: url,
    };
    const message = await post_article(article);
    alert(message);
    dispatch({
      type: POST_ARTICLE,
      payload: message,
    });
  };
};

export const deleteArticle = (id) => {
  return async (dispatch) => {
    const message = await delete_article(id);

    dispatch({
      type: DELETE_ARTICLE,
      payload: message,
    });
    return message;
  };
};

export const updateArticle = (id, article, image) => {
  return async (dispatch) => {
    let url;
    if (image === undefined || image === null) {
      url = article.image;
    } else {
      url = await upload_image(image);
    }
    article = {
      ...article,
      image: url,
    };
    console.log("action update", article);
    const message = await update_article(id, article);
    alert(message);
    dispatch({
      type: UPDATE_ARTICLE,
      payload: message,
    });
    return message;
  };
};
