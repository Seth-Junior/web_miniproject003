import {
  fetch_all_categories,
  post_category,
  update_category,
  delete_category,
  fetch_category_by_id,
} from "../../services/category_service";
import { upload_image } from "../../services/image_service";

export const FETCH_ALL_CATEGORIES = "FETCH_ALL_CATEGORIES";
export const FETCH_CATEGORY_BY_ID = "FETCH_CATEGORY_BY_ID";
export const POST_CATEGORY = "POST_CATEGORY";
export const DELETE_CATEGORY = "DELETE_CATEGORY";
export const UPDATE_CATEGORY = "UPDATE_CATEGORY";

export const fetchAllCategories = () => {
  return async (dispatch) => {
    const categories = await fetch_all_categories();
    console.log("action: ", categories);
    dispatch({
      type: FETCH_ALL_CATEGORIES,
      payload: categories,
    });
  };
};

export const fetchCategory = (id) => {
  return async (dispatch) => {
    const category = await fetch_category_by_id(id);
    dispatch({
      type: FETCH_CATEGORY_BY_ID,
      payload: category,
    });
  };
};

export const postCategory = (category, image) => {
  return async (dispatch) => {
    const url = await upload_image(image);

    category = {
      ...category,
      image: url,
    };

    const message = await post_category(category);
    alert(message);
    dispatch({
      type: POST_CATEGORY,
      payload: message,
    });
  };
};

export const deleteCategory = (id) => {
  return async (dispatch) => {
    const message = await delete_category(id);

    dispatch({
      type: DELETE_CATEGORY,
      payload: message,
    });
  };
};

export const updateCategory = (id, category) => {
  return async (dispatch) => {
    const message = await update_category(id, category);
    dispatch({
      type: UPDATE_CATEGORY,
      payload: message,
    });
  };
};
